<?php
define('TAMANO_MATRIZ', 3);

function generarMatrizCuadrada($tamano) {
    $matriz = array();
    for ($i = 0; $i < $tamano; $i++) {
        $fila = array();
        for ($j = 0; $j < $tamano; $j++) {
            $fila[] = rand(0, 9);
        }
        $matriz[] = $fila;
    }
    return $matriz;
}

function imprimirMatriz($matriz) {
    echo "<table border='1'>";
    foreach ($matriz as $fila) {
        echo "<tr>";
        foreach ($fila as $valor) {
            echo "<td>$valor</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}

function sumaDiagonalPrincipal($matriz) {
    $suma = 0;
    $tamano = count($matriz);
    for ($i = 0; $i < $tamano; $i++) {
        $suma += $matriz[$i][$i];
    }
    return $suma;
}

do {
    $matriz = generarMatrizCuadrada(TAMANO_MATRIZ);
    imprimirMatriz($matriz);
    $sumaDiagonal = sumaDiagonalPrincipal($matriz);
    echo "Suma de la diagonal principal: $sumaDiagonal<br>";

    if ($sumaDiagonal >= 10 && $sumaDiagonal <= 15) {
        echo "La suma de la diagonal está entre 10 y 15. Script terminado.";
        break;
    } else {
        echo "La suma de la diagonal no está en el rango deseado. Generando otra matriz.<br>";
    }
} while (true);

?>
